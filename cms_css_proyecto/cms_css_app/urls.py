from . import views
from django.urls import path

urlpatterns = [
    path('', views.index_page),
    path('about/', views.about_page),
    path('main.html', views.css_style),
]