from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.

# Creamos una vista para que devuelva la pagina principal como vacia
def index_page(request):
    return HttpResponse("Esta es la página principal de mi sitio web. Pregunta por /about o /main.html")


# Creamos una vista para que devuelva la pagina about
def about_page(request):
    content = "Esta es la página 'About' de mi sitio web."
    return render(request, "cms_css/about.html", {"content": content})


# Creamos una vista para que devuelva la pagina con estilo css
def css_style(request):
    css_content = """
    body {
        margin: 10px 20% 50px 70px;
        font-family: sans-serif;
        color: brown;
        background: white;
    }
    """
    return render(request, "cms_css/about.html", {"css_content": css_content})

